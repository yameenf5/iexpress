import { ServerResponse } from "http"

interface LibRequest extends Request{
  params : {[key:string]:string} 
  body? : any 
}

interface LibResponse extends ServerResponse {
  send: <T>(data:T, cb?: Function) => LibResponse;
  status: (code: number) => LibResponse;
  sendFile: (path: string) => LibResponse;
  json: (data: any) => LibResponse;
}

