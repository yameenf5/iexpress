import { server, } from "./lib/express";
import { get, use } from "./lib/methods";

server.listen(3000)

get('/abc/:id/:name', (req, res, next) => {
  const id = req.params.id;
  const name = req.params.name;
  console.log("i am a middleware 1", id, name);
  next();
}, (req, res, next) => {
  const id = req.params.id;
  const name = req.params.name;
  console.log("i am a middleware 2", id, name);
  next();
})

use('/abc/:id/:name', (req, res, next) => {
  console.log("i am a middleware 3");
  next();
})

use((req, res, next) => {
  console.log("i am a middleware 4");
  next()
})

get('/abc/:id/:name', (req, res) => {
  return res.send(`<div>Hello how are you</div>`);
})

