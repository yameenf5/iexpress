import { pathToRegexp } from "path-to-regexp"
import { LibRequest, LibResponse } from "../../types"
import { routes } from "./express";

export const get = (route: string, ...handlers: ((req: LibRequest, res: LibResponse, next: Function) => void)[]) => {
  const keys: { name: string, prefix: string, suffix: string, pattern: string, modifier: string }[] | [] = [];
  const routeToRegex = pathToRegexp(route, keys);
  let paramsIdentifiers: string[] | [] = []
  if (keys.length > 0) {
    paramsIdentifiers = keys.map(key => key.name)
  }
  handlers.forEach(handler => {
    routes.push({ routeRegex: routeToRegex, method: 'GET', params: paramsIdentifiers, handler: handler })
  })
}


export const post = (route: string, ...handlers: ((req: LibRequest, res: LibResponse, next: Function) => void)[]) => {
  const keys: { name: string, prefix: string, suffix: string, pattern: string, modifier: string }[] | [] = [];
  const routeToRegex = pathToRegexp(route, keys);
  let paramsIdentifiers: string[] | [] = []
  if (keys.length > 0) {
    paramsIdentifiers = keys.map(key => key.name)
  }
  handlers.forEach(handler => {
    routes.push({ routeRegex: routeToRegex, method: 'POST', params: paramsIdentifiers, handler: handler })
  })
}

export function use(route: string, ...handlers: ((req: LibRequest, res: LibResponse, next: Function) => void)[]): void;
export function use(...handlers: ((req: LibRequest, res: LibResponse, next: Function) => void)[]): void
export function use(arg1: any, ...arg2: any) {
  if (typeof arg1 === 'string' && typeof arg2 === 'object') {
    const keys: { name: string, prefix: string, suffix: string, pattern: string, modifier: string }[] | [] = [];
    const routeToRegex = pathToRegexp(arg1, keys);
    let paramsIdentifiers: string[] | [] = []
    if (keys.length > 0) {
      paramsIdentifiers = keys.map(key => key.name)
    }
    arg2.forEach((handler: (req: LibRequest, res: LibResponse, next: Function) => void) => {
      routes.push({ routeRegex: routeToRegex, method: 'USE', params: paramsIdentifiers, handler: handler })
    })
  }
  if (typeof arg1 === 'function') {
    const routeToRegex = pathToRegexp('MIDDLEWARE');
    routes.push({ routeRegex: routeToRegex, method: 'USE', params: [], handler: arg1 })
  }
}

