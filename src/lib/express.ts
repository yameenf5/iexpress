import { IncomingMessage, ServerResponse, createServer } from "http"
import { LibRequest, LibResponse } from "../../types";
import { serverRequestWraper, serverResponseWraper } from "./wrapers";
import { match } from "path-to-regexp";

type routes = {
  routeRegex: RegExp,
  method: string,
  params: string[] | [],
  handler: ((req: LibRequest, res: LibResponse, next: Function) => void)
}

type ParamsType = {
  [key: string]: string
} | undefined


export const routes: routes[] = []

export const server = createServer();
server.on('request', (req: Request, res: ServerResponse<IncomingMessage>) => {
  const middleWareStack = routes.filter(route => {
    const match = route.routeRegex.test(req.url)
    if(route.method === 'USE' && match) return true
    if(route.method === 'USE' && route.routeRegex.test('MIDDLEWARE')) return true 
    return (match && req.method === route.method) 
  });
  if (middleWareStack.length > 0) {
    const iexpressReq = serverRequestWraper(req);
    const iexpressRes = serverResponseWraper(res);

    const matchFn = match(middleWareStack[0].routeRegex, { encode: encodeURI });
    const matchedRes = { ...matchFn(req.url) };
    const params = matchedRes.params as ParamsType;
    if (params === undefined) {
      matchedRes.params = {};
    } else {
      let idx = 0;
      for (const key in params) {
        const property = middleWareStack[0].params[idx];
        iexpressReq.params = { ...iexpressReq.params, [property]: params[key] }
        idx++;
      }
    }

    let idx = 0;
    const next = () => {
      idx++;
      if (idx < middleWareStack.length) {
        middleWareStack[idx].handler(iexpressReq, iexpressRes, next);
      }
    }
    middleWareStack[0].handler(iexpressReq, iexpressRes, next);
  }
})






