import { IncomingMessage, ServerResponse } from "http";
import { LibRequest, LibResponse } from "../../types";
import { join } from "path";
import { createReadStream } from "fs";

export function serverResponseWraper(res: ServerResponse<IncomingMessage>): LibResponse {
  const returnVal: LibResponse = res as LibResponse
  returnVal.send = (data, cb) => {
    if (typeof data === 'string')
      res.setHeader('Content-Type', 'text/html');
    if (typeof data === 'object') {
      res.setHeader('Content-Type', 'application/json');
      const dataToJson = JSON.stringify(data);
      res.write(dataToJson);
      if (cb) cb();
      res.end();
      return returnVal;
    }
    res.write(data);
    if (cb) cb();
    res.end();
    return returnVal;
  }

  returnVal.sendFile = (path) => {
    const filePath = join(__dirname, path);
    const readStream = createReadStream(filePath)
    res.setHeader('Content-Type', 'plain/text');
    readStream.pipe(res);
    return returnVal;
  }

  returnVal.status = (code) => {
    res.statusCode = code
    return returnVal
  }

  returnVal.json = (data) => {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(data));
    res.end();
    return returnVal;
  }
  return returnVal
}

export function serverRequestWraper(req: Request): LibRequest{
  const returnVal = req as LibRequest;
  return returnVal;
} 
